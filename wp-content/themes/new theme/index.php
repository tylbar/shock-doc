<?php
/**
 *  Theme:
 *  File: index.php
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>
<?php while(have_posts()): the_post(); ?>
	<section class='posts section grid'>
			<div class="row post-info">
				<img class='user-icon' src="<?php echo get_stylesheet_directory_uri(); ?>/icon/user.png" alt="user icon">
				<p class='author'><?php the_author(); ?></p>
				<p class='time'><?php the_time( 'd/m/y' ); ?></p>
			</div>
			<a href="<?php echo get_permalink(); ?>">
				<?php if( has_post_thumbnail() ): ?>
					<div class="row post-image-container">
						<img src="<?php the_post_thumbnail_url(); ?>" alt="Featured image for '<?php the_title(); ?>' blog post">
					</div>
				<?php endif; ?>
				<div class="row post-excerpt-container">
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
				</div>
			</a>
			<a class='read-more' href="<?php echo get_permalink(); ?>">Read More</a>
			<hr>
	</section>
<?php endwhile; ?>
<div class="navigation grid"><p><?php posts_nav_link('&#8734;','Go
Forward In Time','Go Back in Time'); ?></p></div>
<?php	get_footer(); ?>
