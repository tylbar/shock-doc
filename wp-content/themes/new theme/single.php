<?php
/**
 *  Theme:
 *  File: single.php
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>
<?php while(have_posts()): the_post(); ?>
  <div class="posts section grid">
    <section class="left">
      <div class="row content">
        <h1><?php the_title(); ?></h1>
      	<?php the_content(); ?>
      </div>

      <div class="row post-info">
				<img class='user-icon' src="<?php echo get_stylesheet_directory_uri(); ?>/icon/user.png" alt="user icon">
				<p class='author'><?php the_author(); ?></p>
				<p class='time'><?php the_time( 'd/m/y'); ?></p>
			</div>
      <hr>
      <div class="row single-navigation">
        <?php if(get_adjacent_post(false,'',true)): ?>
        <a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>">Previous</a>
      <?php endif; ?>
      <?php if(get_adjacent_post(false,'',false)): ?>
        <a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>">Next</a>
        <?php endif; ?>
      </div>
    </section>

    <!-- <section class="right">
      <div class="recent-posts">
        <h2>Recent Posts</h2>
        <ul>
          <?php
          	$recent_posts = wp_get_recent_posts();
          	foreach( $recent_posts as $recent ){
          		echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'<br><p>'. date( 'd/m/y', strtotime( $recent['post_date'] ) ) .'</p></a></li> ';
          	}
          ?>
        </ul>
      </div>
    </section> -->


  </div>
<?php endwhile; ?>
<?php get_footer(); ?>
