<?php
/**
 *  Theme:
 *  File: footer.php
 *  Author: Tyler Barnes - http://known.design
 */
 if(!(is_page(14))) {
 get_template_part('template-parts/subfooter');
 }
?>
	</main>
	<footer id="footer" class='clearfix'>
		<section class="grid">
			<div class="left">
        <div class="small-logo-container">
          <img src="<?php the_field('footer_logo', 'option'); ?>" alt="Shock Doc Logo">
  				<p><?php bloginfo('description'); ?></p>
        </div>
			</div>
			<div class="right">
				<div class="column">
					<h3><?php the_field('footer_title_1', 'option'); ?></h3>
					<?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>
				</div>
				<div class="column">
					<h3><?php the_field('footer_title_2', 'option'); ?></h3>
					<?php if(have_rows('footer_column_2', 'option')): ?>
						<ul>
						<?php while(have_rows('footer_column_2', 'option')): the_row();?>
							<li><?php the_sub_field('footer_column_2_line', 'option'); ?></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
				<div class="column">
					<h3><?php the_field('footer_title_3', 'option'); ?></h3>
					<?php if(have_rows('footer_column_3', 'option')): ?>
						<ul>
						<?php while(have_rows('footer_column_3', 'option')): the_row();?>
							<li><a href="<?php the_field('social_link', 'option') ?>"><img src="<?php the_sub_field('social_icon', 'option') ?>" alt="<?php the_sub_field('footer_column_3_line', 'option'); ?>"><?php the_sub_field('footer_column_3_line', 'option'); ?></a></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</section>
    <section class="copyright">
      <p>
        &copy; Shock Doc 2016
        <br><span>&nbsp; | &nbsp;</span>
        Designed &amp; Developed by <a href="http://known.design">Known Design</a>
      </p>
    </section>
	</footer>
</div> <!-- wrapper -->

<script>(function(){var method;var noop=function(){};var methods=['assert','clear','count','debug','dir','dirxml','error','exception','group','groupCollapsed','groupEnd','info','log','markTimeline','profile','profileEnd','table','time','timeEnd','timeStamp','trace','warn'];var length=methods.length;var console=(window.console=window.console||{});while(length--){method=methods[length];if(!console[method]){console[method]=noop;}}}());</script>
<?php wp_footer(); ?>
</body>
</html>
