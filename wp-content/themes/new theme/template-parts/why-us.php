<section class='why-us section'>
	<div class="grid">
		<div class="row">
			<h1><?php the_field('why_mobile_title'); ?></h1>
			<h1><?php the_field('why_title'); ?></h1>
		</div>
		<?php if(have_rows('reasons')): while(have_rows('reasons')): the_row();?>
			<div class="row reasons">
				<div class="column">
					<hr>
					<h2><?php the_sub_field('reason_title'); ?></h2>
					<p class='highlight'><?php the_sub_field('reason_sub_title'); ?></p>
				</div>
				<div class="column">
					<p class='why-reason'><?php the_sub_field('reason_paragraph'); ?></p>
				</div>
				<div class="column">
					<p><?php the_sub_field('reason_link_question'); ?></p>
					<a href="<?php the_sub_field('reason_link'); ?>"><?php the_sub_field('reason_link_text'); ?></a>
				</div>
			</div>
	<?php endwhile; endif; ?>
	</div>
</section>
