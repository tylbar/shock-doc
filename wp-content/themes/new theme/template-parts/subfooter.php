<?php if(!(is_home() )): ?>
<section class='subfooter section' style='background-image: url("<?php the_field("sub_footer_image") ?>")'>
	<div class="grad"></div>
	<div class="grid">
		<h2><?php the_field('sub_footer_title'); ?></h2>
		<?php if(have_rows('sub_footer_buttons')): while(have_rows('sub_footer_buttons')): the_row(); ?>
			<a href="<?php the_sub_field('button_link') ?>" class="button <?php $array = get_sub_field('hollow_button_check'); if( in_array('hollow', $array)): echo 'hollow-button'; endif; ?>"><?php the_sub_field('button_text'); ?></a>
		<?php endwhile; endif; ?>
	</div>
</section>

<?php elseif (is_home() ): ?>

<section class='subfooter section' style='background-image: url("<?php the_field("sub_footer_image", get_option('page_for_posts')) ?>")'>
	<div class="grad"></div>
	<div class="grid">
		<h2><?php the_field('sub_footer_title', get_option('page_for_posts')); ?></h2>
		<?php if(have_rows('sub_footer_buttons', get_option('page_for_posts'))): while(have_rows('sub_footer_buttons', get_option('page_for_posts'))): the_row(); ?>
			<a href="<?php the_sub_field('button_link', get_option('page_for_posts')) ?>" class="button <?php $array = get_sub_field('hollow_button_check', get_option('page_for_posts')); if( in_array('hollow', $array)): echo 'hollow-button'; endif; ?>"><?php the_sub_field('button_text', get_option('page_for_posts')); ?></a>
		<?php endwhile; endif; ?>
	</div>
</section>
<?php endif; ?>
