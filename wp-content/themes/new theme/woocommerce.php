<?php
/**
 *  Theme:
 *  File: woocommerce.php
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>
<div class="grid section woo-template">
  <div class="right">
    <?php woocommerce_content(); ?>
  </div>
  <div class="left">
    <nav class="product-menu">
      <h2>Categories</h2>
      <?php wp_nav_menu( array( 'theme_location' => 'product-categories' ) ); ?>
    </nav>
  </div>
</div>

<?php get_footer(); ?>
