<?php
/**
 *  Theme:
 *  Template Name: About
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>

<section class="about">
  <div class="grid">
    <div class="left">
      <?php if(have_rows('about_text')): while( have_rows('about_text')): the_row(); ?>
        <div class="text-section">
          <h2><?php the_sub_field('title'); ?></h2>
          <p><?php the_sub_field('paragraph'); ?></p>
        </div>
      <?php endwhile; endif; ?>
    </div>
    <div class="right">
      <div class="sponsors">
        <h3><?php the_field('sponsors_title'); ?></h3>
        <?php if(have_rows('sponsors')): ?>
          <ul>
          <?php while(have_rows('sponsors')): the_row(); ?>
            <li><a href="<?php the_sub_field('image_link'); ?>"><img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('image_alt'); ?>"></a></li>
          <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
