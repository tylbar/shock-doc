<?php
/**
 *  Theme:
 *  Template Name: Contact
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>

<section class="contact">
  <div class="grid">
    <div class="left">
      <?php echo do_shortcode('[contact-form-7 id="181" title="Contact"]'); ?>
    </div>
    <div class="right">
      <div class="contact-info">
        <h3><?php the_field('footer_title_2', 'option'); ?></h3>
        <?php if(have_rows('footer_column_2', 'option')): ?>
          <ul>
          <?php while(have_rows('footer_column_2', 'option')): the_row();?>
            <li><?php the_sub_field('footer_column_2_line', 'option'); ?></li>
          <?php endwhile; ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php echo do_shortcode('[wpgmza id="1"]'); ?>
</section>

<?php get_footer(); ?>
