<?php
/**
 *  Theme:
 *  Template Name: Quote
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>



<section class="quote">
  <div class="grid">
    <div class="right">
      <?php if(is_user_logged_in()): ?>
      <h1><?php the_field('get_quote_title'); ?></h1>
      <?php echo do_shortcode('[contact-form-7 id="231" title="Quote"]'); ?>
    <?php else: ?>
      <h1>Please log in or register to help us better process your quote request.</h1>
      <?php echo do_shortcode('[woocommerce_my_account]'); ?>
    <?php endif; ?>
    </div>

    <div class="left">
      <div class="order-process">
        <h3><?php the_field('service_process_title'); ?></h3>
        <?php if(have_rows('service_process_list')): ?>
          <ul>
          <?php while(have_rows('service_process_list')): the_row();?>
            <li><?php the_sub_field('process_step'); ?></li>
          <?php endwhile; ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>



<?php get_footer(); ?>
