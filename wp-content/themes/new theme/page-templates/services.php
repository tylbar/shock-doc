<?php
/**
 *  Theme:
 *  Template Name: Services
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>
<script>
	jQuery(document).ready(function($) {
		var menuTop = $('.service-menu').offset().top;
		$(document).scroll(function() {
		  var y = $(this).scrollTop();
		  if (y > menuTop) {
		    $('.to-top').fadeIn();
		  } else {
		    $('.to-top').fadeOut();
		  }
		});
	});
</script>
	<section id='service-menu' class="service-menu">
		<?php if(have_rows('services')): ?>
		<ul>
			<?php while(have_rows('services')): the_row(); ?>
			<li><a href='#<?php the_sub_field('service_section_title'); ?>'><?php the_sub_field('service_section_title'); ?></a></li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
	</section>
	<section class='services-section section'>
		<div class="grid">
			<?php if(have_rows('services')): while(have_rows('services')): the_row(); ?>
				<div class="service-section-wrapper">
				<div class="row">
					<h1 id='<?php the_sub_field('service_section_title'); ?>'><?php the_sub_field('service_section_title'); ?></h1>
				</div>
			<?php	if(have_rows('services_section')): while(have_rows('services_section')): the_row();
			?>

				<div class="row reasons services">
					<div class="column">
						<hr>
						<h2><?php the_sub_field('service_title'); ?></h2>
						<p class='highlight'><?php the_sub_field('service_sub_title'); ?></p>
					</div>
					<div class="column">
						<?php if(have_rows('service_list')): ?>
						<ol class='dashed why-reason service-list'>
						  <?php while(have_rows('service_list')): the_row(); ?>
							<li><?php the_sub_field('service_list_item'); ?></li>
							<?php endwhile; ?>
						</ol>
						<?php endif; ?>
					</div>
					<div class="column price">
						<p><?php the_sub_field('service_price'); ?></p>
					</div>
				</div>
		<?php endwhile; endif; ?>
		</div>
		<?php endwhile; endif; ?>
		</div>
		<div class="disclaimer"><?php the_field('disclaimer'); ?></div>
	</section>

	<a href='#service-menu' class="to-top">menu &#8593;</a>

<?php get_footer(); ?>
