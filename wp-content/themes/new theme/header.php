<?php
/**
 *  Theme:
 *  File: header.php
 *  Author: Tyler Barnes - http://known.design
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo("charset"); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?php bloginfo('url');?>/favicon.ico">
		<title><?php wp_title(''); ?></title>
		<?php if(is_page('home')): ?>
		<style>
		@media (max-width:900px) {
			.home-header {
				background-image: url('<?php the_field("header_image_mobile"); ?>');
			}
		}

		@media (min-width:900px) {
			.home-header {
				background-image: url('<?php the_field("header_image"); ?>');
			}
		}
		</style>
	<?php endif; ?>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div id="wrapper">
		<header id="header" <?php if(is_page('home')): ?> class='home-header'<?php endif; ?> <?php if (!(is_page('home'))): ?> class='header' <?php endif; ?> style='<?php if(!(is_page('home'))): ?> background-image: url("<?php if(!(is_home())): the_field('header_image'); elseif(is_home()): the_field('header_image', get_option('page_for_posts')); endif; if(is_single()): echo the_post_thumbnail_url(); endif; if(is_page(10)): the_field('header_image', 10); endif; ?>"); <?php endif; ?>'>
			<div id='header-inner'>
				<nav id="primary-navigation" class="site-navigation" role="navigation">
					<div class="primary-nav-inner">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'left',
							'container_id' => 'cssmenu',
							'menu_class' => 'nav-menu, nav-left',
							'walker' => new CSS_Menu_Maker_Walker()
						));
						?>

						<?php if ( get_theme_mod( 'ShockDoc_logo' ) ) : ?>
								<a class='menu-logo' href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'ShockDoc_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
						<?php else : ?>
								<hgroup>
										<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
										<h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
								</hgroup>
						<?php endif; ?>

						<?php
						wp_nav_menu(array(
						  'theme_location' => 'right',
						  'container_id' => 'cssmenu',
							'menu_class' => 'nav-menu, nav-right',
						  'walker' => new CSS_Menu_Maker_Walker()
						));
						?>
					</div>
		    </nav>

				<div class="burg-bar">
					<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home' class="logo-text">
						<img src="<?php echo esc_url( get_theme_mod( 'ShockDoc_logo' ) ); ?>" alt="logo" class='mobile-logo'>
						<h2 class='site-title'><?php bloginfo( 'name' ); ?></h2>
					</a>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/icon/burger.svg" alt="mobile menu button" class="burg">
				</div>

						<?php if(is_page('home')): ?>
							<div class='home-logo-container'>
								<img class='logo' src='<?php the_field('home_logo'); ?>'>
								<h2><?php bloginfo('description'); ?></h2>
								<a class='button main-button' href="<?php the_field('header_button_link'); ?>"><?php the_field('header_button_text'); ?></a>
							</div>
						<?php else : ?>
							<div class="header-content">
								<h1><?php if(is_home()): the_field('header_title', get_option('page_for_posts')); elseif(!(is_home()) && !(is_single())): the_field('header_title'); elseif(is_single()): the_title(); endif; ?></h1>
							</div>
						<?php endif; ?>
			</div>
		</header>

		<nav role="navigation" class="mobile-menu-container">
	    <div class='mobile-menu'>
	      <section class="mobile-menu-header">
					<div class="small-logo-container">
						<img src="<?php the_field('footer_logo', 'option'); ?>" alt="Shock Doc Logo">
						<p><?php bloginfo('description'); ?></p>
					</div>
	      </section>
	      <?php wp_nav_menu( array(
	                                'container' => 'ul',
	                                'theme_location' => 'main'
	                              ) ); ?>
	    </div>
		</nav>

		<main role="main">
