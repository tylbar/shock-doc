<?php
/**
 *  Theme:
 *  File: page.php
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<section class="grid section">
	<?php the_content(); ?>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
