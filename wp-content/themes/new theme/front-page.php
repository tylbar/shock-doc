<?php
/**
 *  Theme:
 *  File: front-page.php
 *  Author: Tyler Barnes - http://known.design
 */

get_header(); ?>

<section class='intro section'>
	<div class="grid">
		<div class="left">
			<h1><?php the_field('intro_title'); ?></h1>
			<p><?php the_field('intro_sub_title'); ?></p>

			<p class='highlight'><?php the_field('intro_question'); ?></p>
			<?php if(have_rows('intro_buttons')): while(have_rows('intro_buttons')): the_row(); ?>
			<a href="<?php the_sub_field('button_link') ?>" class="button <?php $array = get_sub_field('hollow_button_check'); if( in_array('hollow', $array)): echo 'hollow-button'; endif; ?>"><?php the_sub_field('button_text'); ?></a>
		<?php endwhile; endif; ?>
		</div>

		<div class="right">
			<img src="<?php the_field('intro_image'); ?>" alt="<?php the_field('intro_image_description'); ?>">
		</div>
	</div>
</section>

<section class='why-us section'>
	<div class="grid">
		<div class="row">
			<h1><?php the_field('why_mobile_title'); ?></h1>
			<h1><?php the_field('why_title'); ?></h1>
		</div>
		<?php if(have_rows('reasons')): while(have_rows('reasons')): the_row();?>
			<div class="row reasons">
				<div class="column">
					<hr>
					<h2><?php the_sub_field('reason_title'); ?></h2>
					<p class='highlight'><?php the_sub_field('reason_sub_title'); ?></p>
				</div>
				<div class="column">
					<p class='why-reason'><?php the_sub_field('reason_paragraph'); ?></p>
				</div>
				<div class="column">
					<p><?php the_sub_field('reason_link_question'); ?></p>
					<a href="<?php the_sub_field('reason_link'); ?>"><?php the_sub_field('reason_link_text'); ?></a>
				</div>
			</div>
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
