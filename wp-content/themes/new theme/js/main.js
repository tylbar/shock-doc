//menu
jQuery(document).ready(function($) {
  var navToggle = $('.burg');
  var mobileMenuContainer = $('.mobile-menu-container');
  var mobileMenu = $('.mobile-menu');

  navToggle.on('click', function() {
    $('html').css('overflow-y', 'hidden');
    mobileMenuContainer.fadeIn(200);
    mobileMenu.addClass('show-menu');
  });

  mobileMenuContainer.on('click', function(e) {
    if (!$(e.target).hasClass('mobile-menu')) {
      $('html').css('overflow-y', 'auto');
      mobileMenuContainer.delay(500).fadeOut(100);
      mobileMenu.removeClass('show-menu');
    }
  });
});
