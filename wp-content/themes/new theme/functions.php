<?php
/**
 *  Theme:
 *  File: functions.php
 *  Author: Tyler Barnes - http://known.design
 */

 function register_my_menus() {
  register_nav_menus(
    array(
      'left' => __( 'Left Menu' ),
      'right' => __( 'Right Menu' ),
      'product-categories' => __( 'Product Categories' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

function load_jquery() {
   wp_enqueue_script( 'jquery' );
}

add_action( 'wp_enqueue_scripts', 'load_jquery' );

function my_assets() {
  wp_register_style('pt-sans-googleFonts', 'https://fonts.googleapis.com/css?family=PT+Sans+Narrow');
  wp_enqueue_style( 'pt-sans-googleFonts');

  wp_register_style('source-sans-pro-googleFonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200');
  wp_enqueue_style( 'source-sans-pro-googleFonts');

  wp_register_style('style', get_template_directory_uri() . '/style.css');
  wp_enqueue_style('style');

	wp_enqueue_script( 'theme-scripts', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'my_assets' );



 if(function_exists('register_nav_menus')):
     register_nav_menus(array(
         'main' => 'Main Navigation'
     ));
 endif;

 if(function_exists('acf_add_options_page')):
   acf_add_options_page();
  //  acf_add_options_sub_page('Past clients');
  //  acf_add_options_sub_page('Social Media icons');
 endif;

 // add_theme_support('post-thumbnails');

 // hide WP meta from hackers
 remove_action('wp_head', 'wp_generator');
 remove_action('wp_head', 'wlwmanifest_link');

 // add custom variables
 add_action('wp_head','site_url_script');
 function site_url_script() {
  ?>
  <script type="text/javascript">
   var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
   var siteUrl = '<?php echo get_bloginfo('url'); ?>';
  </script>
 <?php
 }

 function debug($var) {
     $bt = debug_backtrace();
     $caller = array_shift($bt);
     echo '<strong>', $caller['file'], ' (line ', $caller['line'], ')</strong>';
     echo '<pre style="background:#fafafa;padding:10px;font-size:13px">';
     print_r($var);
     echo '</pre>';
 }

 function shockdoc_theme_customizer( $wp_customize ) {
   $wp_customize->add_section( 'ShockDoc_logo_section' , array(
     'title'       => __( 'Logo', 'ShockDoc' ),
     'priority'    => 30,
     'description' => 'Upload a logo to replace the default site name and description in the header',
   ) );
   $wp_customize->add_setting( 'ShockDoc_logo' );
   $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ShockDock_logo', array(
     'label'    => __( 'Logo', 'ShockDoc' ),
     'section'  => 'ShockDoc_logo_section',
     'settings' => 'ShockDoc_logo',
   ) ) );
 }
 add_action( 'customize_register', 'shockdoc_theme_customizer' );

 // Hide editor
 add_action( 'admin_head', 'hide_editor' );

 function hide_editor() {
     remove_post_type_support('page', 'editor');
 }


 //allow svg support
 function cc_mime_types($mimes) {
   $mimes['svg'] = 'image/svg+xml';
   return $mimes;
 }
 add_filter('upload_mimes', 'cc_mime_types');

 /*************************
 WEB REVENUE INFINITE SCROLLING
 *************************/
 /*
 http://www.webhostingsecretrevealed.net/blog/wordpress-blog/how-to-create-an-infinite-scrolling-wp-site/
 Function that will register and enqueue the infinite scolling's script to be used in the theme.
 */
 function twentytwelve_script_infinite_scrolling(){
     wp_register_script(
         'infinite_scrolling',//name of script
         get_template_directory_uri().'/js/jquery.infinitescroll.min.js',//where the file is
         array('jquery'),//this script requires a jquery script
         null,//don't have a script version number
         true//script will de placed on footer
     );

     if(!is_singular()){ //only when we have more than 1 post
         //we'll load this script
         wp_enqueue_script('infinite_scrolling');
     }
 }


 //Register our custom scripts!
 add_action('wp_enqueue_scripts', 'twentytwelve_script_infinite_scrolling');

 /*
 Function that will set infinite scrolling to be displayed in the page.
 */
 function set_infinite_scrolling(){

     if(!is_singular()){//again, only when we have more than 1 post
     //add js script below
     ?>
         <script type="text/javascript">
             /*
                 This is the inifinite scrolling setting, you can modify this at your will
             */
             var inf_scrolling = {
                 /*
                     img: is the loading image path, add a nice gif loading icon there
                     msgText: the loading message
                     finishedMsg: the finished loading message
                 */
                 loading:{
                     img: "<? echo get_template_directory_uri(); ?>/img/ajax-loader.gif",
                     msgText: "Loading next posts....",
                     finishedMsg: "Posts loaded!!",
                 },

                 /*Next item is set nextSelector.
                 NextSelector is the css class of page navigation.
                 In our case is #nav-below .nav-previous a
                 */
                 "nextSelector":"#nav-below .nav-previous a",

                 //navSelector is a css id of page navigation
                 "navSelector":"#nav-below",

                 //itemSelector is the div where post is displayed
                 "itemSelector":"article",

                 //contentSelector is the div where page content (posts) is displayed
                 "contentSelector":"#content"
             };

             /*
                 Last thing to do is configure contentSelector to infinite scroll,
                 with a function jquery from infinite-scroll.min.js
             */
             jQuery(inf_scrolling.contentSelector).infinitescroll(inf_scrolling);
         </script>
     <?
     }
 }

 /*
     we need to add this action on page's footer.
     100 is a priority number that correpond a later execution.
 */
 add_action( 'wp_footer', 'set_infinite_scrolling',100 );

 function pagination_nav() {


     ?>
           <section class="pagi">
             <?php if(get_next_posts_link()): ?>
               <a href='<?php $npl=explode('"',get_next_posts_link()); $npl_url=$npl[1]; ?>' class="prev available">Previous post</a>
             <?php else: ?>
               <div class="prev">Previous post</div>
             <?php endif; ?>

             <?php if(get_previous_posts_link()): ?>
               <a href='<?php $npl=explode('"',get_previous_posts_link()); $npl_url=$npl[1]; ?>' class="next available">Next post</a>
             <?php else: ?>
               <div class="next">Next post</div>
             <?php endif; ?>
           </section>
 <?php
 }

//custom excerpt
 function custom_wp_trim_excerpt($text) {
 $raw_excerpt = $text;
 if ( '' == $text ) {
     //Retrieve the post content.
     $text = get_the_content('');

     //Delete all shortcode tags from the content.
     $text = strip_shortcodes( $text );

     $text = apply_filters('the_content', $text);
     $text = str_replace(']]>', ']]&gt;', $text);

     $allowed_tags = '<br>'; /*** MODIFY THIS. Add the allowed HTML tags separated by a comma.***/
     $text = strip_tags($text, $allowed_tags);

     $excerpt_word_count = 55; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
     $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);

     $excerpt_end = '...'; /*** MODIFY THIS. change the excerpt endind to something else.***/
     $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);

     $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
     if ( count($words) > $excerpt_length ) {
         array_pop($words);
         $text = implode(' ', $words);
         $text = $text . $excerpt_more;
     } else {
         $text = implode(' ', $words);
     }
 }
 return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
 }
 remove_filter('get_the_excerpt', 'wp_trim_excerpt');
 add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');


//woocommerce support declaration
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// function pep_add_new_field($enq_fields) {
//    $new_field = array(
//                      /* the id of each field should be unique, do not include spaces or special characters except _ */
//                      'id' => '',
//
//                      /* you can specify a class, to style the element, there are two classes provided wdm-modal_text, wdm-modal_textarea, wdm-modal_radio
//                     To specify multiple classes, add classes separated by a space */
//                     'class' => '',
//
//                     /* specify the type of the field: text, textarea, radio, select, checkbox, multiple*/
//                     'type' => '',
//
//                     /* add a placeholder for a customer making the inquiry */
//                     'placeholder' => '',
//
//                    /* specify if the field is required ‘yes’ or ‘no’ */
//                    'required' => '',
//
//                    /* add a message which has to be displayed, if the required field is not filled */
//                    'required_message' => '',
//
//                    /* specify a regular expression (excluding the leading and trailing /), to validate the field */
//                    'validation' => '',
//
//                    /* add a message which has to be displayed, if there is an error in validation */
//                    'validation_message' => '',
//
//                    /* if the value has to be added to the admin email sent, specify ‘yes’ else ‘no’ */
//                    'include_in_admin_mail' => '',
//
//                    /* if the value has to be added to the email sent to the customer, specify ‘yes’ else ‘no’ */
//                    'include_in_customer_mail' => '',
//
//                    /* add a label for the field, which will be used to save the field in the database */
//                    'label' => '',
//
//                   /* add a default value for text field or textarea */
//                   'value' => '',
//
//                   /* for radio buttons, checkbox, multiple select, dropdown, and select field, specify an array of options */
//                   'options' => ''
//                 );
//
//     // ****** IMPORTANT********
//     // the order of the fields specified will be decide the order in which fields will be displayed
//     $enq_fields = array( $enq_fields, $new_field );
//     return $enq_fields;
// }
//
// /* replace <field_id> with custname, txtemail, txtphone, txtsubject, txtmsg */
// add_filter( 'pep_fields_<field_id>' , 'pep_add_new_field' , 10, 1 );



//redirect after register or login
add_filter( 'woocommerce_login_redirect', 'krex_redirect');
add_filter( 'woocommerce_registration_redirect', 'krex_redirect');

function krex_redirect( $redirect_to ) {

    if ( ! empty( $_REQUEST['_wp_http_referer'] ) ){
        $ref = wp_unslash( $_REQUEST['_wp_http_referer'] );
    }
    return $ref;
}


//css dropdown Menu

class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }

    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'><span>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</span></a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}

//remove query strings from static resources
function _remove_script_version( $src ){
$parts = explode( '?ver', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//remove jquery migrate
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

function remove_jquery_migrate( &$scripts)
{
    if(!is_admin())
    {
        $scripts->remove( 'jquery');
        $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
    }
}
